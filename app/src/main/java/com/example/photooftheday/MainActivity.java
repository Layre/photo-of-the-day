package com.example.photooftheday;

import androidx.appcompat.app.AppCompatActivity;

import android.content.res.AssetManager;
import android.os.Bundle;

import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // ominous android stuff
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // name of the subfolder in the assets directory that contains the photos, used for path to the photos
        final String ASSETS_SUBFOLDER_NAME = "photooftheday";
        // get photo names in an array
        AssetManager am = getAssets();
        String[] photos = new String[0];
        try {
            photos = am.list(ASSETS_SUBFOLDER_NAME);
        } catch (IOException e) {
            e.printStackTrace();
        }
        // calculate days since 1.1.1970 (unix epoch)
        long daysSinceUnixEpoch = System.currentTimeMillis() / 1000 / 60 / 60 / 24;
        // calculate the index for today's photo dynamically, starts again by 0 when all photos have been shown once
        int indexOfCurrentDay = (int) daysSinceUnixEpoch % photos.length;
        // load today's photo and display it in app
        String url = ASSETS_SUBFOLDER_NAME + "/" + photos[indexOfCurrentDay];
        SubsamplingScaleImageView imageView = (SubsamplingScaleImageView) findViewById(R.id.photoViewer); // Uses SubsamplingScaleImageView because normal ImageView crashes with bigger images
        imageView.setImage(ImageSource.asset(url));


    }
}