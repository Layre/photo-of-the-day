# Photo of the Day
## Description
This simple android app displays a different photo each day. In this example it shows different ProgrammerHumor memes curated from the r/ProgrammerHumor subreddit.

**It can easily be modified to display your own images!** 

The images to display are located at ```./app/src/main/assets/photooftheday```.

## APK
If you just want to use the apk, it is located at ```./app/release```.

## Installation of the project
If you want to temper with the whole project, follow these steps:

- download [AndroidStudio](https://developer.android.com/studio/)
- clone the Photo of the Day project.
- Open it in AndroidStudio

## Running
See https://developer.android.com/studio/run

## Building an APK
See https://developer.android.com/studio/run#reference

## Stuff to know
The images to display are located at ```./app/src/main/assets/photooftheday``` and NOT in the drawables folder.

If you want the app to display your own Images simply modify the ```assets/photooftheday``` folder and build your own APK

This app uses [SubsamplingScaleImageView](https://github.com/davemorrissey/subsampling-scale-image-view) by davemorrissey, because the native ImageView crashes with bigger images.



